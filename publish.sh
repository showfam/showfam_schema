#!/bin/sh
#set -x

NUGET_VERSION=1.0.6
PROTO_SRC=protos
GEN_FILES=gen
LIB_SRC=lib
OUT_DIR=artefacts
NUGET_TOOLS=/Users/nkigen/.nuget/packages/grpc.tools/1.22.0/tools/macosx_x64

rm -rf $GEN_FILES $LIB_SRC
mkdir $GEN_FILES $GEN_FILES/csharp
#build and upload c# lib to azure artefact
${NUGET_TOOLS}/protoc  \
     $(find ${PROTO_SRC} -type f -exec echo ''{} \;) ./google/protobuf/timestamp.proto \
     --csharp_out ${GEN_FILES}/csharp --grpc_out ${GEN_FILES}/csharp -I. --plugin=protoc-gen-grpc=${NUGET_TOOLS}/grpc_csharp_plugin


dotnet new classlib --framework=netcoreapp3.0 -o ${LIB_SRC}/ShowFam.Schema
cp ${GEN_FILES}/csharp/* ${LIB_SRC}/ShowFam.Schema/
cd ${LIB_SRC}/ShowFam.Schema
rm Class1.cs
rm Timestamp.cs
dotnet restore
dotnet add package Google.Protobuf -v 3.9.1
dotnet add package Grpc.Core -v 1.22.0 
#dotnet build

#pack 
dotnet pack --output ${OUT_DIR}/nupkgs -p:Version=$NUGET_VERSION

#publish to nuget
nuget push -Source "showfam-internal" ${OUT_DIR}/nupkgs/* -ApiKey AzureDevOps